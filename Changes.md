# Changes

This file is a manually maintained list of changes for each release.
Feel free to add your changes here when sending pull requests.
Also send corrections if you spot any mistakes.

## v0.0.1 (2015-07-22)

* Creating Basic jira fetching api for projects
* @TODO
* :- parse url to check if it contains http or https
* :- try catch for argument list
* :- callback for everything
* :- eventemiiter before fetching start, fetching inprogress, fetching completed
* :- ssl certificate access
* :- restapi authorization auto fetch if username && password given
* :- authorization on expiry recreate to continue the connection
* :- waiting time if url blocks for data fetching overload
